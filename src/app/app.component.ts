import { Component } from '@angular/core';
import { HeaderLinks } from './shared/constants/constants';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  selectedLink = HeaderLinks.RECIPES;
  headerLink = HeaderLinks;

  onNavigate(link: string) {
    this.selectedLink = link;
  }
}
