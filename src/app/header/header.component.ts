import { Component, EventEmitter, Output } from '@angular/core';
import { HeaderLinks } from '../shared/constants/constants';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent {
  collapsed = true;
  headerLink = HeaderLinks;

  @Output() linkSelected = new EventEmitter<string>();

  onSelect(link: string) {
    this.linkSelected.emit(link);
  }
}
